import numpy as np
import matplotlib.pyplot as plt
from matplotlib.text import OffsetFrom

def get_data(in_file):
    file = open(in_file, 'r')
    Lines = file.readlines()
    templist, pointindex_array = [], []
    out_array = np.array([0,0,0])
    for line in Lines:
        splitline = line.split('|')
        if(splitline[0] != 'DATA'):
            continue
        splitdata = splitline[1].split('.')
        if((splitdata[1] == 'DXY') or (splitdata[1] == 'XYInc')):
            continue

        templist.append(splitline[3])
        pointindex = splitline[1].split('.')[0][len('Point'):]
        if (len(pointindex_array)==0) or (pointindex_array[-1] != pointindex):
            pointindex_array.append(pointindex) # info about data point index

    num_points = len(templist)//3
    for i in range(num_points):
        point = 3*i
        temp_array = np.array([templist[point],templist[point+1],templist[point+2]])
        out_array = np.vstack((out_array,temp_array))
    out_array = np.delete(out_array, (0), axis=0)
        
    return out_array, pointindex_array

def get_data_negative(in_file):
    file = open(in_file, 'r')
    Lines = file.readlines()
    templist = []
    out_array = np.array([0,0,0])
    for line in Lines:
        splitline = line.split('|')
        if(splitline[0] != 'DATA'):
            continue
        splitdata = splitline[1].split('.')
        if((splitdata[1] == 'DX') or (splitdata[1] == 'DY')):
            continue
        templist.append(splitline[3])
    num_points = len(templist)/3
    for i in range(num_points):
        point = 3*i
        temp_array = np.array([templist[point],templist[point+1],-1*templist[point+2]])
        out_array = np.vstack((out_array,temp_array))
    out_array = np.delete(out_array, (0), axis=0)
    return out_array

def get_distance(in_file):
    file = open(in_file, 'r')
    Lines = file.readlines()
    templist = []
    for line in Lines:
        splitline = line.split('|')
        if(splitline[0] != 'DATA'):
            continue
        splitdata = splitline[1].split('.')
        if((splitdata[1] == 'X') or (splitdata[1] == 'Y') or (splitdata[1] == 'Z')):
            continue
        templist.append(splitline[3])
    return templist

def plot_angles_and_distance(in_distance,title):
    fig, ax = plt.subplots()
    plt.hlines(43,0,42, colors='blue')
    plt.hlines(43-float(in_distance[0]),float(in_distance[2]),42,colors='green')
    plt.vlines(0,0,43,colors='blue')
    plt.vlines(float(in_distance[2]),0,43-float(in_distance[0]),colors='green')
    plt.xlim(-0.5,3)
    plt.ylim(40,44)
    ax.annotate('Bare Module',
                xy=(0.5, 43), xycoords='data',
                xytext=(0.5, 43.5), textcoords='offset points',
                arrowprops=dict(facecolor='blue', shrink=0.05),
                horizontalalignment='right', verticalalignment='top')
    ax.annotate('Flex',
                xy=(in_distance[2], 40.5), xycoords='data',
                xytext=(1.3, 40.5), textcoords='offset points',
                arrowprops=dict(facecolor='green', shrink=0.05),
                horizontalalignment='left', verticalalignment='top')
    plt.annotate(in_distance[0]+'mm, angle = '+in_distance[1],xy=(1.1, 43-(float(in_distance[0])/2)))
    plt.annotate(in_distance[2]+'mm,',xy=(0.05, 41.8))
    plt.annotate('angle = '+in_distance[3],xy=(0.05, 41.6))
    plt.annotate(s='', xy=(1,43), xytext=(1,43-float(in_distance[0])), arrowprops=dict(arrowstyle='<->'))
    plt.annotate(s='', xy=(0,41.5), xytext=(float(in_distance[2]),41.5), arrowprops=dict(arrowstyle='<->'))
    plt.title(title)
    plt.xlabel('x [mm]')
    plt.ylabel('y [mm]')
    titlesplit = title.split(' ')
    newtitle = ''
    for ti in titlesplit:
        newtitle = newtitle + ti
    plt.savefig(newtitle+'_distanceangle.eps', format='eps')
    plt.show()

def subtract(injig, jig_pointindexes, inobject, obj_pointindexes): 
    # jig, obj is bad names - jig = bottom obj, obj = top obj
    outarray = np.array([0,0,0])
    pointindex_array = []
    # loop over jig - anyway we won't be able to subtract if the point is missing in one of the measurement
    for pointindex in jig_pointindexes:
        # check if the same point exist in obj
        if pointindex not in obj_pointindexes: continue
        # info about VALID  point index
        pointindex_array.append(pointindex) 
        pointindex_injig = jig_pointindexes.index(pointindex)
        pointindex_inobj = obj_pointindexes.index(pointindex)
        
        # store x and y from jig 
        x = float(injig[pointindex_injig][0])
        y = float(injig[pointindex_injig][1])
        # store z = z_obj - z_jig
        z = float(inobject[pointindex_inobj][2]) - float(injig[pointindex_injig][2])
        #print ("\n")
        #print (pointindex)
        #print ("%s - %s = %f"%(inobject[pointindex_inobj][2], injig[pointindex_injig][2], z))
        temparray = np.array([x,y,z])
        outarray = np.vstack((outarray,temparray))
    outarray = np.delete(outarray, (0), axis=0)

    return outarray, pointindex_array

def add(back_points,front_points):
    outarray = np.array([0,0,0])
    for i in range(back_points.shape[0]):
        x = float(back_points[i][0])
        y = float(back_points[i][1])
        z = float(front_points[i][2]) + float(back_points[i][2])
        if(z < 0):
            z = z*(-1)
        temparray = np.array([x,y,z])
        outarray = np.vstack((outarray,temparray))
    outarray = np.delete(outarray, (0), axis=0)
    return outarray

def get_precision(inlisttemp,title,axis):
    inlist = []
    for point in inlisttemp:
        inlist.append(float(point)*1000)
    plt.hist(inlist)
    plt.xlabel(axis+' [$\mu$m]')
    plt.ylabel('Count')
    plt.title(title)
    plt.subplots_adjust(left=0.3)
    me = np.array(inlist).mean()
    sd = np.array(inlist).std()
    med = np.median(np.array(inlist))
    plt.text(0.02, 0.7, 'Mean = '+"%.1f" % me+' $\mu$m', fontsize=14, transform=plt.gcf().transFigure)
    plt.text(0.02, 0.65, 'RMS = '+"%.3f" % sd+' $\mu$m', fontsize=14, transform=plt.gcf().transFigure)
    plt.text(0.02, 0.6, 'Median = '+"%.1f" % med+' $\mu$m', fontsize=14, transform=plt.gcf().transFigure)
    plt.savefig(title+'_precision.eps', format='eps')
    plt.show()
