# Module Metrology

## Flex PCB
- Mass    
- X-Y measurement
- Z measurement
    - 4 pickup areas
    - Capacitor height

## Bare Module
- Mass
- Sensor X-Y measurement and thickness
- FE chip X-Y measurement and thickness
- Bare module thickness
- Bare module flatness

## Assembled Module
- Mass
- Z measurement
    - 4 pickup areas
    - Capacitor height
- Bare module to flex alignment
    - PCB to sensor distance (top, bottom)
    - PCB to FE distance (left, right)
    - Angle
- Flatness    

## How to run plotting scripts
Script ```do_bare_measurements.py``` gives measures of various points on the object in question, including best fit planes, height histograms, point interpolation, and flatness measurements. One can test using files in 'test' folder. 

```
python3 do_bare_measurements.py -s [flex/bare/assembled] -n [index#] -t [title]
```
