import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import lstsq

def plot_contour(xy_array,grid_z0,title, pointindexes):
    fig, ax = plt.subplots()
    ax.plot(xy_array[:,0], xy_array[:,1], 'bo')
    plt.annotate = [ax.text(*item) for item in zip(xy_array[:,0], xy_array[:,1], pointindexes)]
    plt.imshow(grid_z0.T, cmap='jet',extent=(0,41,0,41), origin='lower')
    plt.colorbar()
    plt.clim(-0.1,0.8)
    plt.title(title)
    plt.xlabel('x [mm]')
    plt.ylabel('y [mm]')
    titlesplit = title.split(' ')
    newtitle = ''
    for ti in titlesplit:
        newtitle = newtitle + ti

    #plt.savefig('test/plots/'+newtitle+'_contour.eps', format='eps')
    plt.savefig('test/plots/'+newtitle+'_contour.pdf', format='pdf')
    plt.savefig('test/plots/'+newtitle+'_contour.png', format='png')
    plt.close()

def plot_wire(xy_array,z_array,grid_x,grid_y,grid_z0,title):
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_surface(grid_x, grid_y, grid_z0,cmap=cm.Accent)
    ax.set_xlabel('x [mm]')
    ax.set_ylabel('y [mm]')
    ax.set_zlabel('z [mm]')
    titlesplit = title.split(' ')
    newtitle = ''
    for ti in titlesplit:
        newtitle = newtitle + ti
    plt.title(title)

    #plt.savefig('test/plots/'+newtitle+'_wire.eps', format='eps')
    plt.savefig('test/plots/'+newtitle+'_wire.pdf', format='pdf')
    plt.savefig('test/plots/'+newtitle+'_wire.png', format='png')
    plt.close()

def plot_points(data,title,killz):
    titlesplit = title.split(' ')
    newtitle = ''
    for ti in titlesplit:
        newtitle = newtitle + ti
    z = []
    for x in data:
        if(killz == 1):
            if(float(x[2])<0):
                continue
        z.append(float(x[2]))
    plt.hist(z,bins=10)
    plt.xlabel('z [mm]')
    plt.ylabel('Count')
    #plt.ylim(0,30)
    plt.title(title)
    plt.subplots_adjust(left=0.3)
    me = np.array(z).mean()
    sd = np.array(z).std()
    med = np.median(np.array(z))
    plt.text(0.02, 0.7, 'Mean = '+"%.3f" % me+' mm', fontsize=14, transform=plt.gcf().transFigure)
    plt.text(0.02, 0.65, 'RMS = '+"%.3f" % sd+' mm', fontsize=14, transform=plt.gcf().transFigure)
    plt.text(0.02, 0.6, 'Median = '+"%.3f" % med+' mm', fontsize=14, transform=plt.gcf().transFigure)

    #plt.savefig('test/plots/'+newtitle+'_points.eps', format='eps')
    plt.savefig('test/plots/'+newtitle+'_points.pdf', format='pdf')
    plt.savefig('test/plots/'+newtitle+'_points.png', format='png')
    plt.close()

    return me,sd,med

def plot_fit(data,title,killz):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    xy_array = np.array([0,0])
    z_array = np.array([0])
    xx = []
    y = []
    z = []
    for x in data:
        if(killz == 1):
            if(float(x[2])<0):
                continue
        tempxy = np.array([float(x[0]),float(x[1])])
        xy_array = np.vstack((xy_array,tempxy))
        z_array = np.append(z_array,[float(x[2])])
        xx.append(float(x[0]))
        y.append(float(x[1]))
        z.append(float(x[2]))
    xy_array = np.delete(xy_array, (0), axis=0)
    z_array = np.delete(z_array, (0), axis=0)
    ax.scatter(xx, y, z, c='red')
    tmp_A = []
    for i in range(len(xx)):
        tmp_A.append([xx[i], y[i], 1])
    b = np.matrix(z).T
    A = np.matrix(tmp_A)

    fit, residual, rnk, s = lstsq(A, b)

    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    X,Y = np.meshgrid(np.arange(xlim[0], xlim[1]),
                    np.arange(ylim[0], ylim[1]))
    Z = np.zeros(X.shape)
    for r in range(X.shape[0]):
        for c in range(X.shape[1]):
            Z[r,c] = fit[0] * X[r,c] + fit[1] * Y[r,c] + fit[2]
    ax.plot_wireframe(X,Y,Z, color='k')

    ax.set_xlabel('x [mm]')
    ax.set_ylabel('y [mm]')
    ax.set_zlabel('z [mm]')
    titlesplit = title.split(' ')
    newtitle = ''
    for ti in titlesplit:
        newtitle = newtitle + ti
    plt.title(title)

    #plt.savefig('test/plots/'+newtitle+'_fit.eps', format='eps')
    plt.savefig('test/plots/'+newtitle+'_fit.pdf', format='pdf')
    plt.savefig('test/plots/'+newtitle+'_fit.png', format='png')
    plt.close()

    resid = []
    add = 0
    for i in range(len(z)):
        new_z = (fit[0][0]*xx[i]) + (fit[1][0]*y[i]) + fit[2][0]
        resid.append((new_z - z[i])*1000)
    flat = abs(max(resid)) + abs(min(resid))
    plt.hist(resid,bins=10)
    plt.xlabel('Diff [$\mu$m]')
    plt.ylabel('Count')
    plt.title(title)
    #plt.subplots_adjust(left=0.3)
    #plt.text(0.02, 0.7, 'Flatness = '+"%.1f" % flat+'$\mu$m', fontsize=14, transform=plt.gcf().transFigure)

    #plt.savefig('test/plots/'+newtitle+'_diff.eps', format='eps')
    plt.savefig('test/plots/'+newtitle+'_diff.pdf', format='pdf')
    plt.savefig('test/plots/'+newtitle+'_diff.png', format='png')
    plt.close()

def interpolate(data, title, killz, pointindexes):
    if len(data) != len(pointindexes):        
        print ("\033[91m!!WARNING!! \nData and point index list have different length.\033[0m")
        exit()
    grid_x, grid_y = np.mgrid[0:41:410j, 0:41:410j]
    xy_array = np.array([0,0])
    z_array = np.array([0])
    for x in data:
        if(killz == 1):
            if(float(x[2])<0):
                continue
        tempxy = np.array([float(x[0]),float(x[1])])
        xy_array = np.vstack((xy_array,tempxy))
        z_array = np.append(z_array,[float(x[2])])
    xy_array = np.delete(xy_array, (0), axis=0)
    z_array = np.delete(z_array, (0), axis=0)
    grid_z0 = griddata(xy_array, z_array, (grid_x, grid_y), method='cubic')

    plot_contour(xy_array,grid_z0,title, pointindexes)
    plot_wire(xy_array,z_array,grid_x,grid_y,grid_z0,title)
    plot_fit(data,title,killz)
    plot_points(data,title,killz)
