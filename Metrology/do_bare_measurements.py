#!/usr/bin/python
import sys
import numpy as np
from helpers import *
from interpolate_and_plot import *
from scipy.interpolate import griddata as gd
import argparse

parser = argparse.ArgumentParser(description = "Draw plots")
parser.add_argument('--status', '-s', help='flex/bare/assembled', action='store', type=str, default="flex")
parser.add_argument('--number', '-n', help='index number for flex/bare/assembled', action='store', type=str, default="029")
parser.add_argument('--title', '-t', help='postfix', action='store', type=str, default="test")
args = parser.parse_args()

filePath = "test"
in_jig = "%s/%s/jig.txt"%(filePath, args.status) # We have single jig.txt for all 
pathToInput = "%s/%s/%s%s"%(filePath, args.status, args.status, args.number)
in_vac = "%s/%s.txt"%(pathToInput, args.status)
in_obj = in_vac # Need to figure out why there are two same inputs
title = args.title

# read data from txt files
jig_points, jig_pointindexes = get_data(in_jig)
obj_points, obj_pointindexes = get_data(in_obj)
vac_points, vac_pointindexes = get_data(in_vac)

# artificial removal ################################
for i in [5, 7, 9, 13, 15, 18, 41, 35, 33]:
    idx = jig_pointindexes.index(str(i))
    jig_points = np.delete(jig_points, idx, axis=0)
    del jig_pointindexes[idx]
    if len(jig_points) != len(jig_pointindexes):
        print ("WARNING")
        exit()
#####################################################


interpolate(jig_points,str(title)+' Jig', 0, jig_pointindexes)
interpolate(obj_points,str(title)+ ' Raw', 0, obj_pointindexes)
interpolate(vac_points,str(title)+' Vacuum Raw',0, vac_pointindexes)

module_height, validobj_pointindexes = subtract(jig_points, jig_pointindexes, obj_points, obj_pointindexes)

vacuum_height, validvac_pointindexes = subtract(jig_points, jig_pointindexes, vac_points, vac_pointindexes)
#planarity_height = subtract(vacuum_height, vac_pointindexes, module_height, )

interpolate(module_height, str(title), 0, validobj_pointindexes)
interpolate(vacuum_height, str(title)+' Vacuum', 0, validvac_pointindexes) # this is what we are comparing


'''
#interpolate(planarity_height,str(title)+' Planarity', 0, vac_pointindexes)

distance = get_distance(in_obj)

'''
