#!/usr/bin/python3
import sys
import numpy as np
import re as re
from helpers import *
from interpolate_and_plot import *
from scipy.interpolate import griddata as gd

special_points = [[61, 62, 63, 64], 
                  [55, 56, 57, 58], 
                  [71, 72, 73, 74], 
                  [65, 66, 67, 68]]

def get_pp_data(in_file,ref_points):
    file = open(in_file, 'r')
    Lines = file.readlines()
    templist = []
    out_array = np.array([0, 0, 0])
    for line in Lines:
        splitline = line.split('|')
        if(splitline[0] != 'DATA'):
            continue
        splitdata = splitline[1].split('.')
        if((splitdata[1] == 'DXY') or (splitdata[1] == 'XYInc')):
            continue
        if (int(re.sub("\D", "", splitdata[0])) in ref_points):
            templist.append(splitline[3])
    num_points = len(templist)//3
    for i in range(num_points):
        point = 3*i
        temp_array = np.array(
            [templist[point], templist[point+1], templist[point+2]])
        out_array = np.vstack((out_array, temp_array))
    out_array = np.delete(out_array, (0), axis=0)
    return out_array


def pp_plot(ref_points, text):
    pp_jig_points = get_pp_data(in_jig, ref_points)
    #pp_obj_points = get_pp_data(in_obj, ref_points)
    pp_vac_points = get_pp_data(in_vac, ref_points)

    #pp_module_height = subtract(pp_jig_points,pp_obj_points)
    pp_vacuum_height = subtract(pp_jig_points,pp_vac_points)
    #pp_planarity_height = subtract(pp_vacuum_height,pp_module_height)

    #interpolate(pp_module_height,str(title)+' '+ text, 0)
    interpolate(pp_vacuum_height,str(title)+' Vacuum '+ text, 0)
    #interpolate(pp_planarity_height,str(title)+' Planarity '+ text, 0)

in_jig = sys.argv[1]
in_obj = sys.argv[2]
in_vac = sys.argv[3]
title = sys.argv[4]

def main(argv):
    pp = 1
    for pts in special_points:
        pp_plot(pts, "pickup point " + str(pp))
        pp += 1
        
if __name__ == "__main__":
    main(sys.argv)
