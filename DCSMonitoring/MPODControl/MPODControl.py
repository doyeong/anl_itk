import os, sys
import argparse
import subprocess
import re

parser = argparse.ArgumentParser(description="Script to control Weiner MPod")
parser.add_argument("-ch","--channel",dest="channel",required=True,type=str)
parser.add_argument("-v","--voltage",dest="voltage",required=True,type=float)

#parser = argparse.ArgumentParser(description="Script to control Weiner MPod")
#parser.add_argument("-ch","--channel",dest="channel",required=True,type=str)
#parser.add_argument("-v","--voltage",dest="voltage",required=True,type=float)

#opt = parser.parse_args()
#channel = opt.channel
#voltage = opt.voltage
MPOD_IP = os.getenv('MPOD_IP')
class MPODControl():
    def __init__(self,  channel=0, value=0, voltage=0, current=0):
        self.channel=channel
        self.value=value
        self.voltage=voltage
        self.current=current

    def channel_switch(self, channel, value):
        #os.system("snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru 192.168.0.9 outputVoltage.u{} F 0".format(channel))
        command = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputSwitch.u{} i {}".format(MPOD_IP,channel,value)
        os.system(command)

    def read_voltageRate(self, channel):
        command = "snmpget -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputVoltageRiseRate.u{}".format(MPOD_IP, channel)
        vratestr= subprocess.check_output(command, shell=True)
        vratestr = vratestr.decode('utf-8')
        vrate = re.findall(r'\d+\.\d+', vratestr)
        return float(vrate[0])
         
    def read_voltage(self, channel):
        command = "snmpget -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputVoltage.u{}".format(MPOD_IP, channel)
        vstr= subprocess.check_output(command, shell=True)
        vstr = vstr.decode('utf-8')
        vbias = re.findall(r'\d+\.\d+', vstr)
        return round(float(vbias[0]), 2)
        
    def read_senseVoltage(self, channel):
        command = "snmpget -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputMeasurementSenseVoltage.u{}".format(MPOD_IP, channel)
        vstr= subprocess.check_output(command, shell=True)
        vstr = vstr.decode('utf-8')
        vbias = re.findall(r'\d+\.\d+', vstr)
        return round(float(vbias[0]), 2)

    def read_current(self, channel):
        command = "snmpget -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputCurrent.u{}".format(MPOD_IP, channel)
        #ileakage = os.system(command) 
        istr= subprocess.check_output(command, shell=True)
        istr = istr.decode('utf-8')
        ileakage = re.findall(r'\d+\.\d+', istr)
        return round(float(ileakage[0]), 2)

    def read_measCurrent(self, channel):
        command = "snmpget -Op +2.9 -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputMeasurementCurrent.u{}".format(MPOD_IP, channel)
        #ileakage = os.system(command) 
        istr= subprocess.check_output(command, shell=True)
        istr = istr.decode('utf-8')
        ileakage = re.findall(r'\d+\.\d+', istr)
        return round(float(ileakage[0]), 2)
    
    def read_measIsegCurrent(self, channel):
        command = "snmpget -Op +2.9 -Oqv -v 2c -m +WIENER-CRATE-MIB -c public {} outputMeasurementCurrent.u{}".format(MPOD_IP, channel)
        #ileakage = os.system(command) 
        istr= subprocess.check_output(command, shell=True)
        istr = istr.decode('utf-8')
        ileakage = re.findall(r'\d+\.\d+', istr)
        return round(float(ileakage[0])*1E6, 3)

    def set_voltageRate(self, channel, rate):
        command = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputVoltageRiseRate.u{} F {}".format(MPOD_IP,channel,rate)
        os.system(command)    
        return

    def set_voltage(self, channel, voltage):
        command = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputVoltage.u{} F {}".format(MPOD_IP,channel,voltage)    
        os.system(command)

    def set_current(self, channel, current):
        command = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputCurrent.u{} F {}".format(MPOD_IP,channel,current)    
        os.system(command)

    def set_voltageCurrent(self, channel, voltage, current):
        command1 = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputVoltage.u{} F {}".format(MPOD_IP,channel,voltage)
        command2 = "snmpset -Oqv -v 2c -m +WIENER-CRATE-MIB -c guru {} outputCurrent.u{} F {}".format(MPOD_IP,channel,current)    
        os.system(command1)        
        os.system(command2)

# #Read voltage on that channel
# channel_read(channel)
# #Change voltage of that channel
# channel_changeVoltage(channel,voltage)
# #Read voltage on that channel
# channel_read(channel)
# #Change voltage of that channel to 0
# channel_changeVoltage(channel,0)
# #Read voltage of that channel
# channel_read(channel)
# #Turn off channel
# channel_switch(channel,0)
