from influxdb import InfluxDBClient
from MPODControl.MPODControl import MPODControl
from HMPControl.HMPControlTools import *
import time
import argparse

parser = argparse.ArgumentParser(description = "")
parser.add_argument('--targetTemp', '-t', help='target temperature', action='store', default='-15')
args = parser.parse_args()


client = InfluxDBClient()
measurement_name = 'RD53A-001-RealModule'
database_name = 'dcsDB'
query = f'SELECT * FROM "{measurement_name}" ORDER BY time DESC LIMIT 1'

def readLastPoint():
    # Perform the query
    results = client.query(query, database=database_name)
    return list(results.get_points())[-1]

## define main function
def main():
    ps_peltier = connectRohdeS()
    Nch_pltr = 2
    Pltr_voltage, Pltr_current = 6, 10
    Pltr_voltage_min = 0
    Pltr_voltage_max = 8
    voltage_step = 0.5
    sleeptime = 60
    temp_old = -999.
    
    # Continuous monitoring temperature
    while True:
        temp = readLastPoint()['temperature_moduleN']
        
        if temp!=None:            
            dt_target = float(args.targetTemp) - float(temp)
            if abs(dt_target) < 0.5: voltage_step = 0.0        
            elif abs(dt_target) < 1. : voltage_step = 0.1
            elif abs(dt_target) < 5. : voltage_step = 2
            else : voltage_step = 3

            dt = float(temp) - temp_old
            temp_old = float(temp)
            if abs(dt) < 1: sleeptime = 60
            if abs(dt) > 5: sleeptime = 2
            sleeptime = 2
            print('\ntarget temp: %s \t current temp: %.2f \t at  %s'%(args.targetTemp, temp, time.strftime("%H:%M:%S")))
            print('dt_target = %.2f dt = %.2f'%(dt_target, dt))
            print('voltage step size = %.f'%(voltage_step))
            print('sleep time = %.f'%(sleeptime))
            #print (time.strftime("%H %M %S"))
            if temp<float(args.targetTemp):
                if Pltr_voltage-voltage_step < Pltr_voltage_min:
                    print('We reached to the minimum voltage limit ... let\'s wait more')
                    Pltr_voltage = Pltr_voltage_min
                    PL_voltage = readLastPoint()['PL_voltage']
                    PL_current = readLastPoint()['PL_current']
                    print("voltage reading: %s \t current reading: %s"%(PL_voltage, PL_current))
                    continue
                Pltr_voltage-=voltage_step
                print ('Peltier voltage adjusted from %.1f to %.1f'%(Pltr_voltage+voltage_step, Pltr_voltage))                                
            else:
                if Pltr_voltage+voltage_step > Pltr_voltage_max:
                    print('We reached to the maximum voltage limit ... let\'s wait more')
                    Pltr_voltage = Pltr_voltage_max
                    PL_voltage = readLastPoint()['PL_voltage']
                    PL_current = readLastPoint()['PL_current']
                    print("voltage reading: %s \t current reading: %s"%(PL_voltage, PL_current))
                    continue
                Pltr_voltage+=voltage_step
                print ('Peltier voltage adjusted from %.1f to %.1f'%(Pltr_voltage-voltage_step, Pltr_voltage))
            setVoltCurr(ps_peltier, Nch_pltr, Pltr_voltage, Pltr_current)
            time.sleep(2)

            PL_voltage = readLastPoint()['PL_voltage']
            PL_current = readLastPoint()['PL_current']
            print("voltage reading: %s \t current reading: %s"%(PL_voltage, PL_current))
            time.sleep(sleeptime)
                    
                    
if __name__ == "__main__":
    main()

