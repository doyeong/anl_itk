import sys
import serial
import io
import pyvisa
from pyvisa import constants
sys.path.append('../')
from Chiller.chiller_cf41 import *

onoff = sys.argv[1]
temp = sys.argv[2] 

if __name__ == "__main__":

    #ser = initiate_Serial()
    ser = initiate_chiller()
    
    set_temp(ser, int(temp))
    time.sleep(2)
    
    if "on" in onoff:
        print ("Chiller on")
        chiller_on(ser)
    elif "off" in onoff:
        print ("Chiller off")
        chiller_off(ser)
    else:
        set_temp(ser, float(onoff))
        print ("Please, choose argument among 'on' and 'off'.")
