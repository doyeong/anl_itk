from MPODControl.MPODControl import MPODControl
from HMPControl.HMPControlTools import * 
from Chiller.chiller_cf41 import *
import argparse
from influxdb import InfluxDBClient
import os

client = InfluxDBClient()
measurement_name = 'RD53A-001-RealModule'
database_name = os.getenv('INFLUXDB_DCS')
query = f'SELECT * FROM "{measurement_name}" ORDER BY time DESC LIMIT 1'
dcsPath = '/home/itktestsetupa/ITk/anl_itk/DCSMonitoring'

parser = argparse.ArgumentParser(description = "")
parser.add_argument('--meascurrent',  help='measure LV current', action='store_true', default=False)
parser.add_argument('--measvoltage', help='measure LV voltage', action='store_true', default=False)
parser.add_argument('--poweron', help='power on LV supply', action='store_true', default=False)
parser.add_argument('--poweroff', help='power off LV supply', action='store_true', default=False)
parser.add_argument('--clearEvent', help='clear event', action='store_true', default=False)
parser.add_argument('--setcurrent', help='set I and V for LV supply', action='store', default="NONE")
parser.add_argument('--rampvoltage', help='set I and V for LV supply', action='store', default="NONE")
args = parser.parse_args()

def readLastPoint():
    # Perform the query
    results = client.query(query, database=database_name)
    return list(results.get_points())[-1]

def main():
    mpod = MPODControl()
    channel_HV = os.getenv('HV_CHANNEL1')
    
    if args.meascurrent is True:
        while True:
            LV_current = readLastPoint()['HV_current']
            if LV_current!=None: break
        #print("LV_current: %f"%(LV_current))
        current = float(LV_current*0.000001) if abs(float(LV_current))>0.005 else 0.0
        print(current)
        return current

    elif args.measvoltage is True:
        while True:
            LV_voltage = readLastPoint()['HV_voltage']
            if LV_voltage!=None: break
        #print("LV_voltage: %f"%(LV_voltage))
        print (float(LV_voltage)*(-1.) if abs(float(LV_voltage))>0.045 else 0.0)
        return float(LV_voltage)*(-1.) if abs(float(LV_voltage))>0.045 else 0.0

    elif args.poweron is True:
        mpod.channel_switch(channel_HV,1) #switch LV MPOD ON = 1

    elif args.poweroff is True:
        mpod.channel_switch(channel_HV,0) #switch LV MPOD OFF = 0

    elif args.setcurrent != "NONE":
        setV = float((args.setcurrent).split(',')[0])*(-1.)
        setI = float((args.setcurrent).split(',')[1])
        print ("V %f \t I: %f"%(setV, setI))
        mpod.set_voltageCurrent(channel_HV,setV,setI)

        LV_current = (mpod.read_measCurrent(channel_HV)) #A
        print("LV_current: %f"%(LV_current))
        LV_voltage = mpod.read_senseVoltage(channel_HV) #V
        print("LV_voltage: %f"%(LV_voltage))

    elif args.rampvoltage != "NONE":
        setV = float((args.rampvoltage).split(',')[0])
        setI = float((args.rampvoltage).split(',')[1])
        #print ("V %f \t I: %f"%(setV, setI))
        mpod.set_voltageRate(channel_HV,setV)

        return (setV)

    elif args.clearEvent is True:
        mpod.channel_ClearEvent(channel_HV)

    
if __name__ == "__main__":
    main()
