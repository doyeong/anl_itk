from MPODControl.MPODControl import MPODControl
from HMPControl.HMPControlTools import * 
from Chiller.chiller_cf41 import *
#from Alarmer import *
import argparse
import time

parser = argparse.ArgumentParser(description = "")
parser.add_argument('--voltage', '-v', help='peltier voltage', action='store', default='2.5')
parser.add_argument('--switch', '-s', help='turn [on/off] peltier', action='store', default='NONE')
parser.add_argument('--polarity', '-p', help='[heat/cool]', action='store', default='cool')
args = parser.parse_args()

def main():
    ps_peltier = connectRohdeS()    
    Nch_pltr = 2
    Pltr_voltage = args.voltage
    Pltr_current = 10
    Nch_pltr_polarity = 3
    if args.polarity=='cool':
        peltier_on_off(ps_peltier,Nch_pltr_polarity,1) #switch peltier ON = 1
    else:
        peltier_on_off(ps_peltier,Nch_pltr_polarity,0) #switch peltier OFF = 1
    setVoltCurr(ps_peltier, Nch_pltr, Pltr_voltage, Pltr_current)  #device, channel no, voltage, current
    time.sleep(2)
    if  args.switch=='on': peltier_on_off(ps_peltier,Nch_pltr,1) #switch peltier ON = 1    
    elif args.switch=='off': peltier_on_off(ps_peltier,Nch_pltr,0) #switch peltier ON = 1    

    values=measVoltCurr(ps_peltier, Nch_pltr) #measure voltage and current of channel Nch    
    isFloat = False
    while isFloat:
        values=measVoltCurr(ps_peltier, Nch_pltr) #measure voltage and current of channel Nch    
        if isinstance(float(values[0]), float) and isinstance(float(values[1]), float):
            isFloat = True
            
    PL_voltage = round(float(values[0]), 2) #V
    PL_current = round(float(values[1]), 2) #A    
    print("PL_voltage: ")
    print(PL_voltage)
    print("PL_current: ")
    print(PL_current)
    
if __name__ == "__main__":
    main()
