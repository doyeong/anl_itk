from influxdb import InfluxDBClient
import time

client = InfluxDBClient()
measurement_name = 'RD53A-001-RealModule'
database_name = 'dcsDB'
query = f'SELECT * FROM "{measurement_name}" ORDER BY time DESC LIMIT 1'
dcsPath = '/home/itktestsetupa/ITk/anl_itk/DCSMonitoring/'

def readLastPoint():
    # Perform the query                                                                                                                                                                                                                                                                                                                                                                     
    results = client.query(query, database=database_name)
    return list(results.get_points())[-1]


def getDCS():
    # Continuous monitoring temperature
    while True:
        temp = readLastPoint()['temperature_moduleN']
        if temp!=None: break
    return temp


def main():
    moduleTemp = getDCS()
    print (moduleTemp)
    return moduleTemp


if __name__=="__main__":
    main()






