from MPODControl.MPODControl import MPODControl
from HMPControl.HMPControlTools import * 
from Chiller.chiller_cf41 import *
import argparse
import os, sys

parser = argparse.ArgumentParser(description = "")
parser.add_argument('--meascurrent',  help='measure LV current', action='store_true', default=False)
parser.add_argument('--measvoltage', help='measure LV voltage', action='store_true', default=False)
parser.add_argument('--poweron', help='power on LV supply', action='store_true', default=False)
parser.add_argument('--poweroff', help='power off LV supply', action='store_true', default=False)
parser.add_argument('--setcurrent', help='set I and V for LV supply', action='store', default="NONE")
parser.add_argument('--clearevent', help='clear the LV supply channel after HighI', action='store_true', default=False)
args = parser.parse_args()


def main():
    mpod = MPODControl()
    channel_LV = os.getenv('LV_CHANNEL1')
    
    if args.meascurrent is True: 
        LV_current = (mpod.read_measCurrent(channel_LV)) #mA
        #print("LV_current: %f"%(LV_current))
        print(float(LV_current))
        return float(LV_current)

    elif args.measvoltage is True:
        LV_voltage = mpod.read_senseVoltage(channel_LV) #V
        #print("LV_voltage: %f"%(LV_voltage))
        print(float(LV_voltage))
        return float(LV_voltage)

    elif args.poweron is True:
        mpod.channel_switch(channel_LV,1) #switch LV MPOD ON = 1

    elif args.poweroff is True:
        mpod.channel_switch(channel_LV,0) #switch LV MPOD OFF = 0
        
    elif args.setcurrent != "NONE":
        setV = float((args.setcurrent).split(',')[0])
        setI = float((args.setcurrent).split(',')[1])
        print ("V %f \t I: %f"%(setV, setI))
        mpod.set_voltageCurrent(channel_LV,setV,setI)

        LV_current = (mpod.read_measCurrent(channel_LV)) #mA
        print("LV_current: %f"%(LV_current))
        LV_voltage = mpod.read_senseVoltage(channel_LV) #V
        print("LV_voltage: %f"%(LV_voltage))

    elif args.clearevent is True:
        mpod.channel_switch(channel_LV,0)
        mpod.channel_switch(channel_LV,10) #switch LV MPOD ON = 1

    
if __name__ == "__main__":
    main()
