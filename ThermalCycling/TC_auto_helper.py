import time

## read module temperature continously from Arduino
def reading_log_files(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    for line in lines:
         module_temp = float(line)  
    return module_temp


def log_generator(filename, period=1):
    data = reading_log_files(filename)
    while True:
        time.sleep(period)
        new_data = reading_log_files(filename)
        yield new_data#[len(data):]
        data = new_data

## control relay
def relay(status,device):
    device.write("INST:NSEL 2")
    device.write("APPL 4")
    if (status==True):
        print ("Turning relay ON")
        device.write("OUTP 1")
    else: 
        device.write("OUTP 0")
        print ("Turning relay OFF")

## control pelltier power supply
def pelt(status,device,v_pelt):
    device.write("INST:NSEL 1")
    if (status==True):
        device.write("OUTP 1")
        device.write("APPL {}".format(v_pelt))
        print ("Applying {} V to pelltier".format(v_pelt))
    if (status==False):
        device.write("OUTP 0")    
        print ("Turning pelltier OFF")