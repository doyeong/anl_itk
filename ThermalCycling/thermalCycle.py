import pyvisa
from pyvisa import constants
import time
import serial
import numpy
import os
import argparse
import sys

from TC_auto_helper import *

chiller_path = "../Chiller/"
sys.path.append(chiller_path)
from chiller import *
# from ..Chiller.chiller import *

parser = argparse.ArgumentParser(description="Automated thermal cycling")
# parser.add_argument("t_thresh",type=float,action="store",help="min temp")
parser.add_argument("nCycles",type=int,action="store",help="number of thermal cycles")
# parser.add_argument("soakTime",type=float,action="store",help="soak time")
parser.add_argument("ch_temp",type=float,default=-25,action="store",help="chiller temp")

args = parser.parse_args()


## Do cold cycle
# pelt(True,device,0) ## Turn on pelltier and apply 0V

# i_cycle = 0

# x = log_generator("../DCSMonitoring/temp_vals.txt")

# relay(True,device)

def ramp(max_voltage,mode):
    pelt_volt = 0
    if (mode=="cool"):
        relay(True,device)
        # print("Cooling module")
    else: 
        relay(False,device) 
        # print("Heating module")   
    while(pelt_volt < 1):
        # print ("Slow cool start")
        pelt(True,device,pelt_volt)
        pelt_volt += 0.25
        time.sleep(15)
    while (pelt_volt < max_voltage -1 ):
        # print ("Cooling")
        pelt(True,device,pelt_volt)
        pelt_volt += 0.5 
        time.sleep(15)
    while (pelt_volt < max_voltage):
        # print ("Slow cool ramp down")
        pelt(True,device,pelt_volt)
        pelt_volt += 0.25
        time.sleep(60)
    print ("Start soak time")    
    time.sleep(5*60)
    print ("End soak time")

    while(pelt_volt > max_voltage-1):
        # print ("here 1")
        pelt(True,device,pelt_volt)
        pelt_volt -= 0.25
        time.sleep(15)
    while(pelt_volt > 1):
        # print ("here 2")
        pelt(True,device,pelt_volt)
        pelt_volt -= 0.5 
        time.sleep(15)
    while(pelt_volt > 0):
        # print ("here 3")
        pelt(True,device,pelt_volt)
        pelt_volt -= 0.25
        time.sleep(60)       



def TempCycle (number, coldVoltage=6.2, hotVoltage=4.1):
    for i in range(number):
        print (f"Thermal cycle # {i} started\n\tramping temperature down")
        ramp(coldVoltage,"cool")
        time.sleep(10)
        print (f"\tramping temperature up")
        ramp(hotVoltage,"warm")
        time.sleep(10)
        print ("\tfinished!")
    print ("All thermal cycling finished!")

##Establish connection with the power supply

rm = pyvisa.ResourceManager("@py")
print(rm.list_resources())
device = rm.open_resource("ASRL/dev/ttyACM0::INSTR",baud_rate=9600, data_bits=8, parity=constants.Parity.none, stop_bits=constants.StopBits.one, write_termination="\n",read_termination="\n")


## Turn Chiller On
on_off(1)
## Set chiller power
set_power(-20)
## Set chiller temp
set_temp(args.ch_temp)
## Ensure chiller temp is -25
t_chiller = read_set_temp()
print ("t_chiller:" ,t_chiller)
chiller_temp = read_bath(t_chiller)
print ("here",chiller_temp)
# if (chiller_temp):
#     print ("Chiller is at -25")

# on_off(0)    
## Check if humidity is ~0

# ramp(6.5,"cool")
# ramp(4,"warm")
# pelt(True,device,0) ## Turn on pelltier and apply 0V
while (chiller_temp):
    print ("Doing regular thermal cycle")
    TempCycle(args.nCycles ,4.1,4.6)

    print ("Doing extreme thermal cycle")
    TempCycle(1,6.5,6.1)
    
    break
set_temp(22)
## Ensure chiller temp is -25
t_chiller = read_set_temp()
print ("t_chiller:" ,t_chiller)
chiller_temp = read_bath(t_chiller)
if(chiller_temp):    
    on_off(0)
# while(i_cycle < args.nCycles):
#     for lines in x:
#         # print (lines)
#         t_mod = lines
         
#         if not os.path.exists("pelt_volt.txt"):
#             with open("pelt_volt.txt","w") as f:
#                 f.write("0")
#         with open("pelt_volt.txt","r") as f:
#             pelt_volt = float(f.read())  
        
#         print ("t_mod: ", t_mod, "t_thresh: ", args.t_thresh)
#         print ("pelt_volt: ", pelt_volt)
#         # if ((args.t_thresh-1) < t_mod < (args.t_thresh+1) and pelt_volt <= 4):
            
#         print ("here")    
#         if (t_mod > args.t_thresh):
#             print ("ramping down")
#             relay(True,device)
#             pelt(True,device,pelt_volt)
#             pelt_volt += 2
#             time.sleep(60)

#             with open ("pelt_volt.txt","w") as f:
#                 f.write(str(pelt_volt))
#             # if (t_mod > args.t_thresh+5):
#             #     print ("Slow cold cycling")
#             #     pelt(True,device,pelt_volt)
                
#             # else:    
#             #     print ("Doing Cold cycling")

#         if (abs(t_mod) < abs(args.t_thresh)):
#             print ("ramping up")
#             relay(False,device)
#             pelt(True,device,pelt_volt)
#             pelt_volt += 2
#             time.sleep(60)

#             with open ("pelt_volt.txt","w") as f:
#                 f.write(str(pelt_volt))    

#     i_cycle +=1