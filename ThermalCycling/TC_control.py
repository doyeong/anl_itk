import pyvisa
from pyvisa import constants
import time
import serial
import numpy
import os

def reading_log_files(filename):
    with open(filename, "r") as f:
        lines = f.readlines()
    for line in lines:
         module_temp = float(line)  
    return module_temp


def log_generator(filename, period=1):
    data = reading_log_files(filename)
    while True:
        time.sleep(period)
        new_data = reading_log_files(filename)
        yield new_data#[len(data):]
        data = new_data

def relay(status):
    device.write("INST:NSEL 2")
    device.write("APPL 4")
    if (status==True):
       device.write("OUTP 1")
    else: device.write("OUTP 0")    
            

# # Open txt file produced by Arduino and read module temperature
# in_file = open("/home/felix/Desktop/YarrV1p2p2/Tools/anl_rd53a_testing/DCSMonitor/temp_vals.txt","r")
# lines = in_file.readlines()
# for line in lines:
#     module_temp = line
    
# print ("Module temperature: ", module_temp)

## Set the chiller to -25C

rm = pyvisa.ResourceManager("@py")
# print(rm.list_resources())

#Connect to the pelltier. Additional options enable us to fix the "timeout error" with pyvisa. Do not really know why.
device = rm.open_resource("ASRL/dev/ttyACM4::INSTR",baud_rate=9600, data_bits=8, parity=constants.Parity.none, stop_bits=constants.StopBits.one, write_termination="\n",read_termination="\n")
# # #print the name of the device i.e pelltier
# # #print(device.query("*IDN?"))
# device.write("INST:NSEL 2")
# device.write("APPL 4")
# if (is_relay=="True"):
#     device.write("OUTP 1")
# else: device.write("OUTP 0")
relay(True)
t_thresh = -45
soaktime = 15
device.write("INST:NSEL 1") ## Select pelltier channel
device.write("OUTP 1") 
# device.write("APPL 0")

# Open txt file produced by Arduino and read module temperature
# in_file = open("../DCSMonitoring/temp_vals.txt","r")

x = log_generator("../DCSMonitoring/temp_vals.txt")
# print (x)
for lines in x:
    # print (lines)
    t_module = lines

    # print (module_temp) 
    
    
    # if not os.path.exists("pelt_volt.txt"):
    # with open("pelt_volt.txt","w") as f:
    #     f.write("0")
    with open("pelt_volt.txt","r") as f:
        pelt_volt = float(f.read())


    if ((t_thresh-1) > t_module > (t_thresh+1) and pelt_volt <= 4):
        device.write("APPL {}".format(pelt_volt))
        time.sleep(60)   
        pelt_volt += 0.5

        with open ("pelt_volt.txt","w") as f:
            f.write(str(pelt_volt)) 

    time.sleep(soaktime*60)
    t_thresh = abs(t_thresh)
    # if ()
    # if (module_temp > min_temp and pelt_volt <= 4):
        
    #     print ("module temp: ", module_temp) 
    #     print ("pelt_volt: ", pelt_volt)  
    #     device.write("APPL {}".format(pelt_volt))
    #     time.sleep(60)   
    #     pelt_volt += 0.5

    #     with open ("pelt_volt.txt","w") as f:
    #         f.write(str(pelt_volt))  
               
    
    # if (module_temp < min_temp):
        
    #     with open ("pelt_volt.txt","w") as f:
    #         f.write(str(2))     
    # with open("pelt_volt.txt","r") as f:
    #     pelt_volt = float(f.read())
    #     device.write("APPL {}".format(pelt_volt))
    #     device.write("INST:NSEL 2")
    #     device.write("OUTP 1")
    print ("cold_min: ", cold_min)
    # if (module_temp == min_temp or module_temp < min_temp):
    #     cold_cycle_count += 1        
    # print ("cold_cycle_count: ", cold_cycle_count)
    # device.write("INST:NSEL 1")
    # device.write("OUTP 1")

    
    #     v_pelt = 0.5
    #     print ("module temp: ", module_temp) 
    #     device.write("APPL {}".format(v_pelt+0.1))
    #     print (v_pelt)
    #     if (True):
    #         v_pelt +=0.1
        
#     print ("here")
    # for v in numpy.arange(0,6,0.05):
    #     device.write("APPL {}".format(v))
    #     print ("module temp: ", module_temp, "  pelt volt: ", v) 
#     if (module_temp > min_temp and v_pelt < 6):
# # #         print ("here")
#         device.write("APPL {}".format(v_pelt+0.1))
#         print ("module temp: ", module_temp, "  pelt volt: ", v_pelt)
# #         time.sleep(30)
#     v_pelt += 0.05


# while(1):
#     in_file = open("../DCSMonitoring/temp_vals.txt","r")
#     lines = in_file.readlines()
#     for line in lines:
#         module_temp = float(line)
#     # print (module_temp)    
#     min_temp = -55

#     print (module_temp)
#     v_pelt = 0.5
#     while (module_temp > -55):
#         print ("module temperature: ", module_temp)
    # device.write("APPL {}".format(v_pelt+0.5))
    # print ("pelltier voltage: ", v_pelt+0.5)



# i=0

# while (i<2):

#     device.write("INST:NSEL 2")
#     device.write("OUTP 1")
#     device.write("APPL 4")

#     device.write("INST:NSEL 1") ## Select pelltier channel
#     device.write("OUTP 1") 

#     # Open txt file produced by Arduino and read module temperature
#     in_file = open("/home/felix/Desktop/YarrV1p2p2/Tools/anl_rd53a_testing/DCSMonitor/temp_vals.txt","r")
#     lines = in_file.readlines()
#     for line in lines:
#         module_temp = line
#     while (module_temp > -55):
            
        
#     print ("Module temperature: ", module_temp)
#     i+=1

# # pelt_volt = 3
# # device.write("APPL {}".format(pelt_volt))

# # i=0
# # while(i<10):
# #     print ("Running cycle #",i)
# #     device.write("INST:NSEL 2")
# #     device.write("OUTP 1")
# #     #select the channel (Channel 1: pelltier, Channel 2: Relay)
# #     device.write("INST:NSEL 1") ## Select pelltier channel
# #     device.write("OUTP 1") 
# #     pelt_volt = 1
# #     while (pelt_volt < 8):
# #         device.write("APPL {}".format(pelt_volt))
        

# #         while True:
# #             ser = serial.Serial('/dev/ttyACM1',9600)
# #             b = ser.readline()
# #             string_n = b.decode()
# #             string = string_n.rstrip()
# #             module_temp = string.split()[2]
# #             print(module_temp)
# #             if float(module_temp) > -55:
# #                 print ("here")
# #                 break
# #         pelt_volt += 0.5        
# #     i+=1



    

# #     ## Go to -55 C
# #     device.write("APPL 6.5")
# #     print("Going to sleep for 10 minutes")
# #     time.sleep(10*60)
# #     print("Back! Ready to switch now!")

# #     ## Turn off relay and go to +60 C
# #     device.write("INST:NSEL 2")
# #     device.write("OUTP 0")
# #     device.write("INST:NSEL 1")
# #     device.write("APPL 6")
# #     time.sleep(3.5*60)
# #     device.write("APPL 5.8")
# #     time.sleep(1*60)
# #     i+=1
