import pyvisa
from pyvisa import constants
import time
import serial
import numpy
import os
import argparse

from TC_auto_helper import *

parser = argparse.ArgumentParser(description="Automated thermal cycling")
parser.add_argument("t_thresh",type=float,action="store",help="min temp")
parser.add_argument("nCycles",type=float,action="store",help="number of thermal cycles")
parser.add_argument("soakTime",type=float,action="store",help="soak time")

args = parser.parse_args()

##Connect to the power supply
rm = pyvisa.ResourceManager("@py")
print(rm.list_resources())
device = rm.open_resource("ASRL/dev/ttyACM2::INSTR",baud_rate=9600, data_bits=8, parity=constants.Parity.none, stop_bits=constants.StopBits.one, write_termination="\n",read_termination="\n")

i_cycle = 0

# relay(True,device) ## Turn on relay
pelt(True,device,0) ## Turn on pelltier and apply 0V

## Start reading module temperature from Arduino
x = log_generator("../DCSMonitoring/temp_vals.txt")
while(i_cycle < args.nCycles):
    for lines in x:
        # print (lines)
        t_mod = lines
         
        if not os.path.exists("pelt_volt.txt"):
            with open("pelt_volt.txt","w") as f:
                f.write("0")
        with open("pelt_volt.txt","r") as f:
            pelt_volt = float(f.read())  
        
        print ("t_mod: ", t_mod, "t_thresh+5: ", args.t_thresh+5)
        print ("pelt_volt: ", pelt_volt)
        # if ((args.t_thresh-1) < t_mod < (args.t_thresh+1) and pelt_volt <= 4):
            
            
        if (t_mod > args.t_thresh):
            relay(True,device)
            if (t_mod > args.t_thresh+5):
                print ("Slow cold cycling")
                pelt(True,device,pelt_volt)
                
            else:    
                print ("Doing Cold cycling")
        #     pelt(True,device,pelt_volt)
        #     time.sleep(30)
        #     pelt_volt += 0.25
        # #     time.sleep(30)

        #     with open ("pelt_volt.txt","w") as f:
        #         f.write(str(pelt_volt))
        # if (t_mod > args.t_thresh + 10):          
        # elif (t_mod < (args.t_thresh+7) and t_mod > (args.t_thresh-7)):
        #     # relay(True,device)
        #     print ("Doing slow cold cycling")
        #     pelt(True,device,pelt_volt)
        #     pelt_volt += 0.05
        #     time.sleep(2*60)

        #     with open ("pelt_volt.txt","w") as f:
        #         f.write(str(pelt_volt))


        
        # elif (t_mod < (args.t_thresh+1) and t_mod > (args.t_thresh-1)):
        #     print ("Reached cold extreme")
        #     time.sleep(10*60)
        
        # # # if ((args.t_thresh-1) < t_mod < (args.t_thresh+1)):
                
                      
        
        # if (t_mod < abs(args.t_thresh) ):
        #     print ("Doing warm cycling")
        #     relay(False,device)
        #     pelt(True,device,pelt_volt)
        #     time.sleep(60)
        #     pelt_volt += 0.25

        #     with open ("pelt_volt.txt","w") as f:
        #         f.write(str(pelt_volt)) 
        # if (abs(args.t_thresh-5) < t_mod < abs(args.t_thresh+5)):
        #     relay(False,device)
        #     print ("Doing slow warm old cycling")
        #     pelt(True,device,pelt_volt)
        #     time.sleep(3*60)
        #     pelt_volt += 0.1

        #     with open ("pelt_volt.txt","w") as f:
        #         f.write(str(pelt_volt))

        #     if (abs(args.t_thresh-1) < t_mod < abs(args.t_thresh+1)):
        #         print ("Reached warm extreme")
        #         time.sleep(3*60)
             
               

        # if (t_mod )          
        # break           

    i_cycle += 1
# while(i_cycle < args.nCycles):
#     relay(True,device) ## Turn on relay
#     pelt(True,device,0) ## Turn on pelltier and apply 0V
 
#     ## Start reading module temperature from Arduino
#     x = log_generator("../DCSMonitoring/temp_vals.txt")
#     print ("here")
#     for lines in x:
#         print (lines)
#         t_mod = lines
         
#         if not os.path.exists("pelt_volt.txt"):
#             with open("pelt_volt.txt","w") as f:
#                 f.write("0")
#         with open("pelt_volt.txt","r") as f:
#             pelt_volt = float(f.read())  
        
#         if ((args.t_thresh-1) > t_mod > (args.t_thresh+1) and pelt_volt <= 4):
#             pelt(True,device,pelt_volt)
#             time.sleep(60)
#             pelt_volt += 0.2

#             with open ("pelt_volt.txt","w") as f:
#                 f.write(str(pelt_volt))  
#         break

      



#     i_cycle += 1


