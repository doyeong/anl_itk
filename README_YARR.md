# ANL_ITk

## How to setup YARR

YARR is the readout system used for ITk pixel modules. 
The gitlab repository can be found [here](https://gitlab.cern.ch/YARR/YARR/-/tree/devel) and more information about the installation and usage can be found [here](https://yarr.web.cern.ch/yarr/).
Here are the YARR installation directions:
- Check the gcc version 
```
 g++ --version
g++ (GCC) 7.3.1 20180303 (Red Hat 7.3.1-5)

```
It should be higher than 9.0
- Follow the directions [here](https://gist.github.com/nchaigne/ad06bc867f911a3c0d32939f1e930a11#build-and-install-gcc) to update the GCC version.

### Configuring YARR

Follow the [online](https://yarr.web.cern.ch/yarr/) documentation for set-up.

You need to first source the Xilinx Vivado settings shell script to setup environment variables for the Vivado installation you're using. This is done with

```source /opt/Xilinx/Vivado_Lab/Vivado_Lab/2019.2/settings.sh```

Once that is done, you can flash the firmware using [this](https://yarr.web.cern.ch/yarr/pcie/#flashing-the-firmware) script which prompts inputs from the list of available options. For use at ANL, we use the following options:

FPGA card:

```
0) tef1001_R1
1) tef1001_R2 <---- Trenz card, Revision 2
2) xpressk7_160
3) xpressk7_325
```

Chip type:
```
0) rd53 <---- RD53A/B modules
1) fei4
2) fe65p2
```
FMC card:
```
0) ohio <---
1) vhdci
```

Link speed:
```
0) 640Mbps <----
1) 160Mbps

```

Channel config:
```
0) 4x4 <---- 4 channels total, 4 lanes per channel; For RD53A
1) 16x1 <---- 16 channels total, 1 lane per channel; For RD53B/ITk pix
```


## How to perform a scan

```
./bin/scanConsole -r <controller file> -c <module config file> -s <scan> -n 1 -m 1

For eg: Performing a digital scan on an ITk pix digital module: ./bin/scanConsole -r configs/controller/specCfg-rd53b-16x1.json -c configs/ITkPix_Digital/connectivity.json -s configs/scans/rd53b/std_digitalscan.json -n 1 -m 1
```
As a result of running the above command, you should see 4 files, namely Chip1.json, Chip2.json, Chip3.json and Chip4.json in the configs/ITkPix_Digital folder. Then, you need to change the "ChipID" in the chip json files to: [reference](atlaswiki.lbl.gov:/pixels/rd53bQuad/index)

```
Chip 1 --> 12
Chip 2 --> 13
Chip 3 --> 14
Chip 4 --> 15
```
- On the power supply, set the voltage to 2.4 V and current to 6A.


## To make plots from YARR scans

```
bin/plotFromDir -i data/last_data -P png
```


