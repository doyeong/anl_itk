import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker

voltage = []
current = []
R = 0.11
vsense = []
module_num = input("Enter the module number: ")
inputfile = "module"+module_num+".txt"
print(inputfile)
f = open(str(inputfile), "r")
next(f)
for row in f:
    row = row.split(',')
    voltage.append(round(float(row[0]),2))
    current.append(round(float(row[1]),2))
    vsense = np.asarray(voltage) - (np.asarray(current)*R)
#print(current)
print(voltage)
print(vsense)
plt.plot(current, vsense, 'ro')
#plt.gca().invert_xaxis()
#plt.gca().invert_yaxis()
plt.xlabel('Current (A)')
plt.ylabel('Voltage (V)')
#plt.invert_xaxis()
#plt.invert_yaxis()
#plt.xlim([0,5])
#plt.ylim([0,2.3])
#plt.xticks(np.arange(0,5,0.5))
#plt.yticks(np.arange(0,2.3,0.2))
#plt.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
#plt.show()

plt.savefig("module"+module_num+"_plot.pdf")
