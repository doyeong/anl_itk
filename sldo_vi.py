import os
from influxdb import InfluxDBClient
import time
import datetime
import subprocess
import re
import numpy as np
import matplotlib.pyplot as plt
def read_info(vddd_voltage, vddd_current, hv_voltage, hv_current):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'vddd_voltage': float(vddd_voltage),
            'vddd_current': float(vddd_current),
            'hv_voltage': float(hv_voltage),
            'hv_current': float(hv_current)
        }
    }]
    return data_list
module_name = input("module name: ")
f = open("module"+module_name +".txt", "w")

#client = InfluxDBClient(host='localhost',port=8086)
#client.switch_database('dcsDB')

# while True:
#     ## check which channel you are using on PS and update according
#     str_vddd_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u100", shell=True)
#     str_vddd_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u100", shell=True)
#     str_hv_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u300", shell=True)
#     str_hv_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u300", shell=True)
#     ##print(str_hv_voltage)

#     vddd_voltage = re.findall(r"\d*\.\d+", str_vddd_voltage.decode('utf-8').split('Float: ')[1])[0]
#     vddd_current = re.findall(r"\d*\.\d+", str_vddd_current.decode('utf-8').split('Float: ')[1])[0]
#     hv_voltage = re.findall(r"([+-]?\d*\.\d+)", str_hv_voltage.decode('utf-8').split('Float: ')[1])[0]
#     hv_current = re.findall(r"\d*\.\d+", str_hv_current.decode('utf-8').split('Float: ')[1])[0]
#     client.write_points(read_info(vddd_voltage, vddd_current, hv_voltage, hv_current))
#     time.sleep(5)
#     print(f"{vddd_voltage} {vddd_current}")
max_current = 5.0
f.write("Voltage(V), Current(A)")
f.write("\n")
voltage_arr = []
current_arr = []
for current in np.arange(5.0, 0.0, -0.1):
    #print(round(current, 2))
    str_setcurrent = subprocess.check_output(f"snmpset -v 2c -m WIENER-CRATE-MIB -c guru 192.168.200.50 outputCurrent.u100 F {round(current, 2)}", shell=True)
    time.sleep(1)
    str_getcurrent = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u100", shell=True)
    str_getvoltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u100", shell=True)
   # print(f"{str_setcurrent}\n{str_getvoltage}\n {str_getcurrent}")   

    voltage = re.findall(r"\d*\.\d+", str_getvoltage.decode('utf-8').split('Float: ')[1])[0]
    current = re.findall(r"\d*\.\d+", str_getcurrent.decode('utf-8').split('Float: ')[1])[0]
    print(f"{voltage} {current}")
    voltage_arr.append(voltage)
    current_arr.append(current)
    f.write(str(voltage)+", "+str(current))
    #f.write("blah")
    f.write("\n")

f.close()
#plt.plot(voltage,current)
#plt.xlabel("Voltage [V]")
#plt.ylabel("Current [A]")
#plt.savefig("module"+module_name+"_plot.pdf")
