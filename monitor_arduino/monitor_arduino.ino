#include <Wire.h>
#include "SHTSensor.h"

SHTSensor sht;

float R0 = 100000.;
float V1;
float R1, logR1;
float Vin = 5;
float T[0];
float c1 = 298.15, b1=3070., b2 = 3380.;
//int pinUsed[3] = {A7, A4, A5};
int V_lidswitch;
int IsPressureSwitch;
int IsVacSwitch;
int count_fails = 0;
bool IsOkay = 1;
int count_stable = 0;

float gettemp(){
  V1=(5/1023.0)*analogRead(A3);
  //Serial.print(V1);
  R1 = R0 *(Vin/V1 - 1.0);
  logR1 = log(R1/R0);
  T[0] = (1.0 / (1/c1 + ((1/3380.)*logR1)))-275.15;  
  return T[0]; //T[0];
}

float getlidswitch(){
   V_lidswitch = digitalRead(A13);
   V_lidswitch = abs(V_lidswitch-1);
   return V_lidswitch;
}

int getPressureSwitch(){
   IsPressureSwitch = digitalRead(42);
   return IsPressureSwitch;
}

int getVacSwitch(){
   IsVacSwitch = digitalRead(40);
   return IsVacSwitch;
}

void setup()
{
 Wire.begin(); // join i2c bus (address optional for master)
 Serial.begin(9600); // start serial for output
 delay(1000);
 pinMode(A9, OUTPUT);
 digitalWrite(A9, HIGH); //Buzzer
 pinMode(A10, OUTPUT);
 digitalWrite(A10 , HIGH); //Green
 pinMode(A11, OUTPUT);
 digitalWrite(A11 , HIGH); //Red
 pinMode(A12, OUTPUT);
 digitalWrite(A12 , HIGH); //Yellow
 pinMode(11, OUTPUT);
 digitalWrite(11, HIGH); //Buzzer
 sht.init();
}
 
void loop()
{

 float moduletemp;
 
 float Humidity;
 float Temperature;
 float dew_point;
 float H;
 int IsPressureON;
 int IsVacON;
 int IsLidClosed;
 
 while(true)
 {

 //delay(10);
 if(sht.readSample()){
 Temperature = sht.getTemperature();
 Humidity = sht.getHumidity();}
 moduletemp = gettemp();
 IsPressureON = getPressureSwitch();
 IsVacON = getVacSwitch();
 IsLidClosed = getlidswitch();  

 if(IsPressureON==1&&IsVacON==1&&IsLidClosed==1&&abs(Temperature-17.5)<2&&abs(Humidity-0)<0.2&&abs(moduletemp-15.5)<2&&IsOkay){
 //if(IsPressureON==1&&IsVacON==1&&IsLidClosed==1){
  digitalWrite(A10 , LOW);
  count_stable++;
  if(count_fails>0) count_fails=0;
  if(count_stable>10) digitalWrite(A12 , HIGH);
 }
 else if(count_stable>10){
   if(IsPressureON==0||IsVacON==0||IsLidClosed==0||abs(Temperature-17.5)>5||abs(Humidity-0)>0.2||abs(moduletemp-17.5)>5){
    count_fails++;
    //Serial.print(count_fails);
    //Serial.print("\n");
    if(count_fails<=5){
      digitalWrite(A12 , LOW);
      digitalWrite(A10 , HIGH);
    }
    else if(count_fails>5){
      digitalWrite(A11 , LOW);
      digitalWrite(A9, LOW);
      digitalWrite(A12 , HIGH);
      //digitalWrite(11, LOW); 
      IsOkay = 0;
    } 
  }
  else if(IsOkay){
    count_fails = 0;
    digitalWrite(A10 , LOW);
    digitalWrite(A12 , HIGH);
    digitalWrite(A11 , HIGH);
    digitalWrite(A9, HIGH);    
  }
  else{
    digitalWrite(A12 , LOW);
  }
 }
 else{
  digitalWrite(A12 , LOW);
  digitalWrite(A10 , HIGH);
  }
   
 if (Humidity == 0.0){
  H = (log10(Humidity+0.5)-2)/0.4343 + (17.62*Temperature)/(243.12+Temperature);
  dew_point = 243.12*H/(17.62-H);
 }
 else{
  H = (log10(Humidity)-2)/0.4343 + (17.62*Temperature)/(243.12+Temperature);
  dew_point = 243.12*H/(17.62-H);
 }
 
 Serial.print(Temperature);
 Serial.print("\t");
 Serial.print(Humidity);
 Serial.print("\t");
 Serial.print(moduletemp);
 Serial.print("\t");
 Serial.print(dew_point);
 Serial.print("\t");
 Serial.print(IsPressureON);
 Serial.print("\t");
 Serial.print(IsVacON);
 Serial.print("\t");
 Serial.print(IsLidClosed);
 Serial.print("\t");
 Serial.print(IsOkay);
 Serial.print("\n");
 delay(1500);
 
 return;
}
}
