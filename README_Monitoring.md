# ANL_ITk


## How to start temp/humidity monitoring
Follow these steps to start the temp/humidity/dew point etc monitoring via the Arduino:
- Ensure the Arduino USB connection to the setup computer is established.
- Navigate to the Arduino application on the computer and open it. Load the "monitor_dew" program. Click on "Tools" in the top menu and ensure that the correct port is selected. It's name is indicated by "Arduino Mega or Mega 2560". 
- Open the Terminal application and do:
```
cd Desktop/YarrV1p2p2/Tools/anl_rd53a_testing/DCSMonitor/

```
- Ensure that in the script "Temp_Humid_dp.py", the port number on L43 matches the one in the Arduino program. Otherwise, the monitoring will not work.
- Often times, one would have to re-plug the Arduino USB when the monitoring stops working. When this happens, check the name of the port in the Arduino program again and ensure that it matches with the one in the "Temp_Humid_dp.py" script".
- In the Arduino app, click on the verify, followed by the upload symbol. If there are no errors this means we are ready to start monitoring. If there are errors, try to unplug the USB and repeat the above steps.
- Go to the terminal application and do:
```
cd Desktop/YarrV1p2p2/Tools/anl_rd53a_testing/DCSMonitor/
python3 Temp_Humid_dp.py

```
When the script runs successfully, four numbers are printed. They are: Air Temperature, Humidity, Module Temperature, Dew Point.
- Open firefox and open the [grafana monitoring webpage](http://localhost:3000/d/zlhvslUWk/anl_dcs_monitoring?orgId=1&refresh=5s).
- In case of error opening the webpage, one needs to restart the grafana server by performing the following steps in the Terminal:
```
cd /home/felix
sudo systemctl start grafana-server
```
- And everything should be up and running for the monitoring now!









